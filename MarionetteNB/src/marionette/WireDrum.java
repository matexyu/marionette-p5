/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marionette;

import marionette.ragdoll.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import processing.core.PApplet;

/**
 *
 * @author Mathieu
 */
public class WireDrum {
  
  class BodyPartId {
    public static final int CABEZA = 0;
    public static final int CUELLO = 1;
    public static final int HOMBRO = 2;
    public static final int BRAZO = 3;
    public static final int ANTEBRAZO = 4;
    public static final int MUNECA = 5;
    public static final int RODILLA = 6;
    public static final int MAX_ID = 7;
  }
  
  class Color{
    public Color(int r, int g, int b, int a)
    {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
    }
    public Color(int r, int g, int b)
    {
      this(r, g, b, 255);
    }
    int r, g, b, a;
  };
  
  HashMap<Integer, Color> mPartToColor = new HashMap<>();
  void initBodyPartColor()
  {
    mPartToColor.put(BodyPartId.CABEZA, new Color(214, 169, 121));
    mPartToColor.put(BodyPartId.CUELLO, new Color(0, 0, 0));
    mPartToColor.put(BodyPartId.HOMBRO, new Color(3, 44, 228));
    mPartToColor.put(BodyPartId.BRAZO, new Color(255, 124, 65));
    mPartToColor.put(BodyPartId.ANTEBRAZO, new Color(230, 33, 0));
    mPartToColor.put(BodyPartId.MUNECA, new Color(255, 64, 255));
    mPartToColor.put(BodyPartId.RODILLA, new Color(0, 143, 22));
  }
  
  Color getPartColor(int body_part_id)
  {
    if(mPartToColor.keySet().contains(body_part_id))
    {
      return mPartToColor.get(body_part_id);
    }
    return new Color(0, 0, 0);
  }
  
  class WireOutlet {
    WireOutlet(int body_part_id, float x, float z)
    {
      this.bodyPartId = body_part_id;
      this.x = x;
      this.z = z;
      color = getPartColor(body_part_id);
    }
    float x;
    float z;
    int bodyPartId;
    Color color;
  };
  
  ArrayList<WireOutlet> mWireOutlets = new ArrayList<>();
  
  void addWireOutlet(int body_part_id, float x_cm_design, float z_step, boolean mirror_x)
  {
    final float num_z_steps = 8.0f;
    final float z_step_size_meters = (mDiameter / num_z_steps);
    float z = z_step * z_step_size_meters;
    float x = mDiameter * (x_cm_design / 30.0f);
    mWireOutlets.add(new WireOutlet(body_part_id, x, z));
    if(mirror_x)
    {
      mWireOutlets.add(new WireOutlet(body_part_id, -x, z));
    }
  }
  private void initWireOutlets()
  {
    initBodyPartColor();
    // (BACK TO FRONT)
    
    // cuello, muñeca
    float zCM = -2; 
    // hombros
    float zH = -1;
    // brazo, antebrazo, cabeza
    float zBAC = 1; 
    // rodilla
    float zR = 2;
    
    addWireOutlet(BodyPartId.CABEZA,    0,     zBAC, false); // 2 tiros, 1 motor
    addWireOutlet(BodyPartId.CUELLO,    9,     zCM,  true);
    addWireOutlet(BodyPartId.HOMBRO,    5.6f,  zH,   true);
    addWireOutlet(BodyPartId.BRAZO,     10,    zBAC, true);
    addWireOutlet(BodyPartId.ANTEBRAZO, 12.3f, zBAC, true);
    addWireOutlet(BodyPartId.MUNECA,    11,    zCM,  true);
    addWireOutlet(BodyPartId.RODILLA,   10.6f, zR,   true);
  }
  
  public WireDrum(float diameter, float elevation)
  {
    mDiameter = diameter;
    mElevation = elevation;
    initWireOutlets();
  }
  
  public void draw(PApplet p5)
  {
    p5.pushStyle();
    p5.pushMatrix();
    p5.translate(0, mElevation, 0);
    // Main Cylinder
    p5.fill(255, 245, 220);
    p5.noStroke();
    Utils.drawCylinder(p5, mDiameter*0.5f, mDiameter*0.5f, 0.05f, 64, true, false);
    
    // Wire outlets
    final float wire_outlet_radius = 0.01f;
    for(WireOutlet wo : mWireOutlets)
    {
      p5.pushMatrix();
      p5.translate(wo.x, -0.05f * 0.5f, wo.z);
      p5.fill(wo.color.r, wo.color.g, wo.color.b);
      p5.noStroke();
      Utils.drawCylinder(p5, wire_outlet_radius, wire_outlet_radius, 0.04f, 32, true, true);
      p5.noFill();
      p5.stroke(wo.color.r, wo.color.g, wo.color.b, 128);
      p5.line(0, 0, 0, 
              0, -mElevation, 0);
      p5.popMatrix();
    }
    p5.popMatrix();
    p5.popStyle();
  }
  
  float mElevation;
  float mDiameter;
}
