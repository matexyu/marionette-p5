/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marionette.ragdoll;

import com.bulletphysics.collision.shapes.CapsuleShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.Point2PointConstraint;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
import processing.core.PApplet;

/**
 *
 * @author Mathieu
 */
public class CapsuleRope {

  DynamicsWorld mOwnerWorld;

  class RopeElement {

    public CollisionShape mShape;
    public RigidBody mBody;
    public Transform mTransform;
    float mLength;
    float mMass;
    Point3d mMidPoint;

    private RopeElement(Point3d point_a, Point3d point_b, float part_mass, boolean is_kinematic)
    {
      mPointA = new Vector3f(point_a);
      mPointB = new Vector3f(point_b);

      mLength = (float) point_a.distance(point_b);
      mTransform = new Transform();

      mShape = new CapsuleShape(0.05f, mLength);

      mMidPoint = new Point3d();
      mMidPoint.interpolate(point_a, point_b, 0.5);

      mTransform.setIdentity();
      mTransform.origin.set(mMidPoint);

      mBody = Utils.localCreateRigidBody(mOwnerWorld, part_mass, mTransform, mShape, is_kinematic);
    }

    public Vector3f mPointA;
    public Vector3f mPointB;

  };

  RopeElement[] mElements;
  float mRopeLength;

  CapsuleRope(DynamicsWorld owner_world, Point3d beginning, Point3d end, float rope_mass, int num_capsules)
  {
    mOwnerWorld = owner_world;
    mRopeLength = (float) beginning.distance(end);
    mElements = new RopeElement[num_capsules];

    float part_mass = rope_mass / (float) num_capsules;

    Point3d pos_a = new Point3d();
    Point3d pos_b = new Point3d();

    for (int i = 0; i < mElements.length; i++)
    {
      double ratio_a = i / (mElements.length - 1.0);
      double ratio_b = (i + 1.0) / (mElements.length - 1.0);
      pos_a.interpolate(beginning, end, ratio_a);
      pos_b.interpolate(beginning, end, ratio_b);

      float modified_mass = part_mass;
      mElements[i] = new RopeElement(pos_a, pos_b, modified_mass, (i == 0));

      if (i > 0)
      {
        // http://bulletphysics.org/Bullet/phpBB3/viewtopic.php?t=7137
        Point2PointConstraint p2p;
        p2p = new Point2PointConstraint(
                mElements[i - 1].mBody,
                mElements[i].mBody,
                mElements[i - 1].mPointB,
                mElements[i].mPointA);

        p2p.setting.damping = 1.0f;// 1.0f
        p2p.setting.impulseClamp = 0.5f;  // 0.0f
        p2p.setting.tau = 0.3f; // 0.3f

        mOwnerWorld.addConstraint(p2p);

      }
    }
  }

  void draw(PApplet p5)
  {
    p5.fill(255);
    p5.stroke(64);

    for (RopeElement e : mElements)
    {
      RigidBody rb = e.mBody;

      Transform trans = new Transform();
      // Get the shape's Transformation
      rb.getMotionState().getWorldTransform(trans);
      // Now we get its transformation matrix
      Matrix4f out = new Matrix4f();
      trans.getMatrix(out);

      p5.pushMatrix();
      p5.scale(1, -1, -1);
      // Apply that matrix before drawing the box
      p5.applyMatrix(out.m00, out.m01, out.m02, out.m03,
                     out.m10, out.m11, out.m12, out.m13,
                     out.m20, out.m21, out.m22, out.m23,
                     out.m30, out.m31, out.m32, out.m33);
      p5.fill(200, 200, 200);
      p5.box(0.02f,
             e.mLength,
             0.02f);
      p5.popMatrix();
    }
  }

  boolean mIsfirstInteraction = true;

  public void setKinematicRopePosition(Transform transform)
  {
    RigidBody head = mElements[0].mBody;
    head.getMotionState().setWorldTransform(transform);
    if (mIsfirstInteraction)
    {
      Utils.doCleanup(head);
      mIsfirstInteraction = false;
    }
  }

}
