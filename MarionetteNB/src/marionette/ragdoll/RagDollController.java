/*

 https://github.com/davidB/jmbullet/blob/master/src/test/java/com/bulletphysics/demos/genericjoint/RagDoll.java

 */
package marionette.ragdoll;

import com.bulletphysics.util.ObjectArrayList;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.DynamicsWorld;

import com.bulletphysics.linearmath.Transform;

import javax.vecmath.Vector3f;
import javax.vecmath.Matrix4f;

import processing.core.PApplet;

public class RagDollController {

  DynamicsWorld dynamicsWorld;

  public RagDollController(DynamicsWorld owner_world)
  {
    dynamicsWorld = owner_world;
  }

  boolean pause = false;

  ObjectArrayList<RagDoll> ragdolls = new ObjectArrayList<>();

  public void spawnRagdoll()
  {
    spawnRagdoll(false);
  }

  float mRagDollScale = 0.75f;

  public void spawnRagdoll(boolean random)
  {
    RagDoll ragDoll = new RagDoll(dynamicsWorld, new Vector3f(0f, mRagDollScale, 0f), mRagDollScale);
    ragdolls.add(ragDoll);
  }

  public void draw(PApplet p5)
  {
    if(ragdolls.size() < 1) return;
    p5.stroke(0);
    p5.noFill();
    p5.box(400);

    p5.fill(255);
    p5.stroke(64);

    {
      Transform ht = new Transform();
      ht.setIdentity();
      ht.origin.set(
              PApplet.map(p5.mouseX, 0, p5.width, -2.0f, 2.0f),
              PApplet.map(p5.mouseY, 0, p5.height, 2.0f, -2.0f),
              0f);
      ragdolls.get(0).setHeadTransform(ht);
    }

    for (RagDoll rd : ragdolls)
    {
      for (RigidBody rb : rd.bodies)
      {
        Transform trans = new Transform();
        // Get the shape's Transformation
        rb.getMotionState().getWorldTransform(trans);
        // Now we get its transformation matrix
        Matrix4f out = new Matrix4f();
        trans.getMatrix(out);

        p5.pushMatrix();
        // Apply that matrix before drawing the box
        p5.applyMatrix(out.m00, out.m01, out.m02, out.m03,
                       out.m10, out.m11, out.m12, out.m13,
                       out.m20, out.m21, out.m22, out.m23,
                       out.m30, out.m31, out.m32, out.m33);
        p5.fill(200, 200, 200);
        p5.box(mRagDollScale * 0.1f,
               mRagDollScale * 0.3f,
               mRagDollScale * 0.1f);
        p5.popMatrix();
      }
    }

  }

}
