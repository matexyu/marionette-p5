/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marionette.ragdoll;

import com.bulletphysics.collision.dispatch.CollisionFlags;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import processing.core.PApplet;

/**
 *
 * @author Mathieu
 */
public class Utils {

  public static RigidBody localCreateRigidBody(DynamicsWorld owner_world, float mass, Transform startTransform,
                                               CollisionShape shape, boolean isKinematicObject)
  {
    boolean isDynamic = (mass != 0f);

    Vector3f localInertia = new Vector3f();
    localInertia.set(0f, 0f, 0f);
    if (isDynamic)
    {
      shape.calculateLocalInertia(mass, localInertia);
    }

    DefaultMotionState myMotionState = new DefaultMotionState(startTransform);
    RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(mass, myMotionState, shape, localInertia);
    rbInfo.additionalDamping = true;
    RigidBody body = new RigidBody(rbInfo);

    if (isKinematicObject)
    {
      body.setCollisionFlags(body.getCollisionFlags() | CollisionFlags.KINEMATIC_OBJECT);
      body.setActivationState(CollisionObject.DISABLE_DEACTIVATION);
      doCleanup(body);
    }

    owner_world.addRigidBody(body);

    return body;
  }

  public static void doCleanup(RigidBody rb)
  {
    Transform t = new Transform();
    rb.getWorldTransform(t);
    rb.setInterpolationWorldTransform(t);
    rb.setInterpolationLinearVelocity(new Vector3f());
    rb.setInterpolationAngularVelocity(new Vector3f());
  }

  public static void drawCylinder(PApplet p5, float topRadius, float bottomRadius, float tall, int sides, boolean cap_top, boolean cap_bottom)
  {
    float angle = 0;
    float angleIncrement = PApplet.TWO_PI / sides;
    p5.beginShape(PApplet.QUAD_STRIP);
    for (int i = 0; i < sides + 1; ++i)
    {
      float cs = PApplet.cos(angle);
      float sn = PApplet.sin(angle);
      p5.normal(cs, 0, sn);
      p5.vertex(topRadius * cs, 0, topRadius * sn);
      p5.vertex(bottomRadius * cs, tall, bottomRadius * sn);
      angle += angleIncrement;
    }
    p5.endShape();

    // If it is not a cone, draw the circular top cap
    if (cap_top)
    {
      angle = 0;
      p5.beginShape(PApplet.TRIANGLE_FAN);
      p5.normal(0, 1, 0);
      // Center point
      p5.vertex(0, 0, 0);
      for (int i = 0; i < sides + 1; i++)
      {
        p5.vertex(topRadius * PApplet.cos(angle), 0, topRadius * PApplet.sin(angle));
        angle += angleIncrement;
      }
      p5.endShape();
    }

    // If it is not a cone, draw the circular bottom cap
    if (cap_bottom)
    {
      angle = 0;
      p5.beginShape(PApplet.TRIANGLE_FAN);
      p5.normal(0, -1, 0);
      // Center point
      p5.vertex(0, tall, 0);
      for (int i = 0; i < sides + 1; i++)
      {
        p5.vertex(bottomRadius * PApplet.cos(angle), tall, bottomRadius * PApplet.sin(angle));
        angle += angleIncrement;
      }
      p5.endShape();
    }
  }
}
