package marionette.ragdoll;

import marionette.input.LeapMotionHands;

import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;

import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.constraintsolver.ConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;

import com.bulletphysics.linearmath.Transform;

import javax.vecmath.Vector3f;
import marionette.Stopwatch;
import marionette.WireDrum;

import peasy.PeasyCam;

import processing.core.PApplet;

public class P5Wrapper {

  PeasyCam cam;

  DiscreteDynamicsWorld dynamicsWorld;

  RagDollController mRagDollController;
  CapsuleRope mCapsuleRope;
  WireDrum mWireDrum;

  LeapMotionHands mLeapMotionHands;

  Stopwatch mTimer = new Stopwatch();
  boolean mIsSimulationPaused = false;
  
  public void setup(PApplet p5, int width, int height)
  {
    p5.size(width, height, PApplet.P3D);
    p5.frameRate(60);

    float screen_height_to_meters = 2.0f;
    float fov = PApplet.PI / 3.0f;
    float cameraZ = (screen_height_to_meters / 2.0f) / PApplet.tan(fov / 2.0f);
    p5.perspective(fov, width / (float) height,
                   cameraZ / 10.0f, cameraZ * 10.0f);
    
    

    mLeapMotionHands = new LeapMotionHands(p5);

    cam = new PeasyCam(p5, 3);
    //cam.lookAt(0, -0.2, 0);
    cam.setResetOnDoubleClick(true);
    cam.setYawRotationMode();

    mWireDrum = new WireDrum(0.30f * 2.0f, 1.5f);

    dynamicsWorld = createDynamicsWorld();

    mRagDollController = new RagDollController(dynamicsWorld);
    // Setup a big ground box
    {
      CollisionShape groundShape = new BoxShape(new Vector3f(20f, 1f, 20f));
      Transform groundTransform = new Transform();
      groundTransform.setIdentity();
      groundTransform.origin.set(0f, -2f, 0f);
      Utils.localCreateRigidBody(dynamicsWorld, 0f, groundTransform, groundShape, false);
    }

    mRagDollController.spawnRagdoll();
  }

  DiscreteDynamicsWorld createDynamicsWorld()
  {
    // Setup the basic world
    DefaultCollisionConfiguration collision_config = new DefaultCollisionConfiguration();
    CollisionDispatcher dispatcher = new CollisionDispatcher(collision_config);
    BroadphaseInterface overlappingPairCache = new DbvtBroadphase();
    ConstraintSolver constraintSolver = new SequentialImpulseConstraintSolver();

    DiscreteDynamicsWorld world = new DiscreteDynamicsWorld(dispatcher, overlappingPairCache, constraintSolver, collision_config);

    world.setGravity(new Vector3f(0f, -9.8f, 0f));

    return world;
  }

  public void draw(PApplet p5)
  {
    p5.pushMatrix();

    if (p5.frameCount == 1)
    {
      p5.frame.setLocation(8, 8);
      cam.setMinimumDistance(0.5);
      cam.setDistance(3);
      cam.setMaximumDistance(10);
      cam.setWheelScale(0.02);
    }

    if (mIsSimulationPaused == false)
    {
      double dt = mTimer.getDeltaTimeMicroseconds();
      double minFPS = 30.0;
      if (dt > 1.0 / minFPS)
      {
        dt = 1.0 / minFPS;
      }
      dynamicsWorld.stepSimulation((float) dt, 10, 1.f / 240.0f);
    }

    p5.background(128);

    p5.pushMatrix();
    p5.pushStyle();
    //p5.translate(0.5f * -p5.displayWidth, 0.5f * -p5.displayHeight);
    p5.translate(-1f, -1.25f);
    p5.scale(2.0f / (float)p5.displayWidth);
    
    p5.stroke(255);
    p5.fill(255);
    p5.strokeWeight(p5.displayWidth * 10.0f);
    mLeapMotionHands.draw(p5);
    p5.popStyle();
    p5.popMatrix();

    p5.lights();

    p5.pushMatrix();
    {
      p5.scale(1, -1, -1);

      // Floor box
      p5.pushMatrix();
      p5.translate(0, -1.05f, 0);
      p5.scale(2, 0.1f, 2);
      p5.box(1);
      p5.popMatrix();

      // World box
      p5.pushStyle();
      p5.stroke(0);
      p5.noFill();
      p5.box(2);
      p5.popStyle();

      mWireDrum.draw(p5);

      mRagDollController.draw(p5);
    }
    p5.popMatrix();
    
    p5.popMatrix();
  }

  float mViewAngleX;

  public void keyPressed(char key, int keyCode)
  {
    if (key == PApplet.CODED)
    {
      if (keyCode == PApplet.UP)
      {
        mViewAngleX += 0.2f;
      }
      else if (keyCode == PApplet.DOWN)
      {
        mViewAngleX -= 0.2f;
      }
    }
  }

}
