package marionette.ragdoll;

import com.bulletphysics.BulletGlobals;

import com.bulletphysics.collision.shapes.CapsuleShape;
import com.bulletphysics.collision.shapes.CollisionShape;

import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;

import com.bulletphysics.dynamics.constraintsolver.Generic6DofConstraint;
import com.bulletphysics.dynamics.constraintsolver.TypedConstraint;

import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;

import javax.vecmath.Vector3f;

public class RagDoll {

  boolean mHasRigidJoint = false;

  final int BODYPART_PELVIS = 0;
  final int BODYPART_SPINE = 1;
  final int BODYPART_HEAD = 2;
  final int BODYPART_LEFT_UPPER_LEG = 3;
  final int BODYPART_LEFT_LOWER_LEG = 4;
  final int BODYPART_RIGHT_UPPER_LEG = 5;
  final int BODYPART_RIGHT_LOWER_LEG = 6;
  final int BODYPART_LEFT_UPPER_ARM = 7;
  final int BODYPART_LEFT_LOWER_ARM = 8;
  final int BODYPART_RIGHT_UPPER_ARM = 9;
  final int BODYPART_RIGHT_LOWER_ARM = 10;
  final int BODYPART_COUNT = BODYPART_RIGHT_LOWER_ARM + 1;

  final int JOINT_PELVIS_SPINE = 0;
  final int JOINT_SPINE_HEAD = 1;
  final int JOINT_LEFT_HIP = 2;
  final int JOINT_LEFT_KNEE = 3;
  final int JOINT_RIGHT_HIP = 4;
  final int JOINT_RIGHT_KNEE = 5;
  final int JOINT_LEFT_SHOULDER = 6;
  final int JOINT_LEFT_ELBOW = 7;
  final int JOINT_RIGHT_SHOULDER = 8;
  final int JOINT_RIGHT_ELBOW = 9;
  final int JOINT_COUNT = JOINT_RIGHT_ELBOW + 1;

  private DynamicsWorld ownerWorld;

  private final CollisionShape[] shapes = new CollisionShape[BODYPART_COUNT];
  private final TypedConstraint[] joints = new TypedConstraint[JOINT_COUNT];

  public RigidBody[] bodies = new RigidBody[BODYPART_COUNT];

  private void doCleanup(RigidBody rb)
  {
    Transform t = new Transform();
    rb.getWorldTransform(t);
    rb.setInterpolationWorldTransform(t);
    rb.setInterpolationLinearVelocity(new Vector3f());
    rb.setInterpolationAngularVelocity(new Vector3f());
  }

  boolean mIsfirstInteraction = true;

  void setHeadTransform(Transform transform)
  {
    RigidBody head = bodies[BODYPART_HEAD];
    head.getMotionState().setWorldTransform(transform);
    if (mIsfirstInteraction)
    {
      doCleanup(head);
      mIsfirstInteraction = false;
    }
  }

  public RagDoll(DynamicsWorld ownerWorld, Vector3f positionOffset)
  {
    this(ownerWorld, positionOffset, 1.0f);
  }

  public RagDoll(DynamicsWorld ownerWorld, Vector3f positionOffset, float scale_ragdoll)
  {
    this.ownerWorld = ownerWorld;

    Transform tmpTrans = new Transform();
    Vector3f tmp = new Vector3f();

    Vector3f rigid_angluar_limits_a = new Vector3f();
    Vector3f rigid_angluar_limits_b = new Vector3f();

    rigid_angluar_limits_a.set(-BulletGlobals.SIMD_EPSILON,
                               -BulletGlobals.SIMD_EPSILON,
                               -BulletGlobals.SIMD_EPSILON);
    rigid_angluar_limits_b.set(BulletGlobals.SIMD_EPSILON,
                               BulletGlobals.SIMD_EPSILON,
                               BulletGlobals.SIMD_EPSILON);

    // Setup the geometry
    shapes[BODYPART_PELVIS] = new CapsuleShape(scale_ragdoll * 0.15f, scale_ragdoll * 0.20f);

    shapes[BODYPART_SPINE] = new CapsuleShape(scale_ragdoll * 0.15f, scale_ragdoll * 0.28f);

    shapes[BODYPART_HEAD] = new CapsuleShape(scale_ragdoll * 0.10f, scale_ragdoll * 0.05f);

    shapes[BODYPART_LEFT_UPPER_LEG] = new CapsuleShape(scale_ragdoll * 0.07f, scale_ragdoll * 0.45f);

    shapes[BODYPART_LEFT_LOWER_LEG] = new CapsuleShape(scale_ragdoll * 0.05f, scale_ragdoll * 0.37f);

    shapes[BODYPART_RIGHT_UPPER_LEG] = new CapsuleShape(scale_ragdoll * 0.07f, scale_ragdoll * 0.45f);

    shapes[BODYPART_RIGHT_LOWER_LEG] = new CapsuleShape(scale_ragdoll * 0.05f, scale_ragdoll * 0.37f);

    shapes[BODYPART_LEFT_UPPER_ARM] = new CapsuleShape(scale_ragdoll * 0.05f, scale_ragdoll * 0.33f);

    shapes[BODYPART_LEFT_LOWER_ARM] = new CapsuleShape(scale_ragdoll * 0.04f, scale_ragdoll * 0.25f);

    shapes[BODYPART_RIGHT_UPPER_ARM] = new CapsuleShape(scale_ragdoll * 0.05f, scale_ragdoll * 0.33f);

    shapes[BODYPART_RIGHT_LOWER_ARM] = new CapsuleShape(scale_ragdoll * 0.04f, scale_ragdoll * 0.25f);

    // Setup all the rigid bodies
    Transform offset = new Transform();
    offset.setIdentity();
    offset.origin.set(positionOffset);

    float part_mass = 0.1f;

    Transform transform = new Transform();
    transform.setIdentity();
    transform.origin.set(0f, scale_ragdoll * 1f, 0f);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_PELVIS] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_PELVIS]);

    transform.setIdentity();
    transform.origin.set(0f, scale_ragdoll * 1.2f, 0f);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_SPINE] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_SPINE]);

    transform.setIdentity();
    transform.origin.set(0f, scale_ragdoll * 1.6f, 0f);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_HEAD] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_HEAD], true); // <------------ KINEMATIC

    transform.setIdentity();
    transform.origin.set(-0.18f * scale_ragdoll, 0.65f * scale_ragdoll, 0f);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_LEFT_UPPER_LEG] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_LEFT_UPPER_LEG]);

    transform.setIdentity();
    transform.origin.set(-0.18f * scale_ragdoll, 0.2f * scale_ragdoll, 0f);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_LEFT_LOWER_LEG] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_LEFT_LOWER_LEG]);

    transform.setIdentity();
    transform.origin.set(0.18f * scale_ragdoll, 0.65f * scale_ragdoll, 0f);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_RIGHT_UPPER_LEG] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_RIGHT_UPPER_LEG]);

    transform.setIdentity();
    transform.origin.set(0.18f * scale_ragdoll, 0.2f * scale_ragdoll, 0f);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_RIGHT_LOWER_LEG] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_RIGHT_LOWER_LEG]);

    transform.setIdentity();
    transform.origin.set(-0.35f * scale_ragdoll, 1.45f * scale_ragdoll, 0f);
    MatrixUtil.setEulerZYX(transform.basis, 0, 0, BulletGlobals.SIMD_HALF_PI);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_LEFT_UPPER_ARM] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_LEFT_UPPER_ARM]);

    transform.setIdentity();
    transform.origin.set(-0.7f * scale_ragdoll, 1.45f * scale_ragdoll, 0f);
    MatrixUtil.setEulerZYX(transform.basis, 0, 0, BulletGlobals.SIMD_HALF_PI);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_LEFT_LOWER_ARM] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_LEFT_LOWER_ARM]);

    transform.setIdentity();
    transform.origin.set(0.35f * scale_ragdoll, 1.45f * scale_ragdoll, 0f);
    MatrixUtil.setEulerZYX(transform.basis, 0, 0, -BulletGlobals.SIMD_HALF_PI);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_RIGHT_UPPER_ARM] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_RIGHT_UPPER_ARM]);

    transform.setIdentity();
    transform.origin.set(0.7f * scale_ragdoll, 1.45f * scale_ragdoll, 0f);
    MatrixUtil.setEulerZYX(transform.basis, 0, 0, -BulletGlobals.SIMD_HALF_PI);
    tmpTrans.mul(offset, transform);
    bodies[BODYPART_RIGHT_LOWER_ARM] = localCreateRigidBody(part_mass, tmpTrans, shapes[BODYPART_RIGHT_LOWER_ARM]);

    // Setup some damping on the m_bodies
    for (int i = 0; i < BODYPART_COUNT; ++i)
    {
      bodies[i].setDamping(0.1f, 0.1f);
      bodies[i].setDeactivationTime(0.8f);
      bodies[i].setSleepingThresholds(1.6f, 2.5f);
    }

    ///////////////////////////// SETTING THE CONSTRAINTS /////////////////////////////////////////////7777
    // Now setup the constraints
    Generic6DofConstraint joint6DOF;
    Transform localA = new Transform(), localB = new Transform();

    boolean useLinearReferenceFrameA = true;

    /// ******* SPINE HEAD ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      localA.origin.set(0f, 0.30f * scale_ragdoll, 0f);

      localB.origin.set(0f, -0.14f * scale_ragdoll, 0f);

      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_SPINE],
              bodies[BODYPART_HEAD],
              localA, localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_PI * 0.3f,
                -BulletGlobals.FLT_EPSILON,
                -BulletGlobals.SIMD_PI * 0.3f);
        joint6DOF.setAngularLowerLimit(tmp);

        tmp.set(BulletGlobals.SIMD_PI * 0.5f,
                BulletGlobals.FLT_EPSILON,
                BulletGlobals.SIMD_PI * 0.3f);
        joint6DOF.setAngularUpperLimit(tmp);
      }

      joints[JOINT_SPINE_HEAD] = joint6DOF;

      ownerWorld.addConstraint(joints[JOINT_SPINE_HEAD], true);

    }
    /// ******* LEFT SHOULDER ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      localA.origin.set(-0.2f * scale_ragdoll, 0.15f * scale_ragdoll, 0f);

      MatrixUtil.setEulerZYX(localB.basis, BulletGlobals.SIMD_HALF_PI, 0, -BulletGlobals.SIMD_HALF_PI);

      localB.origin.set(0f, -0.18f * scale_ragdoll, 0f);

      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_SPINE],
              bodies[BODYPART_LEFT_UPPER_ARM],
              localA, localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_PI * 0.8f,
                -BulletGlobals.FLT_EPSILON,
                -BulletGlobals.SIMD_PI * 0.5f);
        joint6DOF.setAngularLowerLimit(tmp);

        tmp.set(BulletGlobals.SIMD_PI * 0.8f,
                BulletGlobals.FLT_EPSILON,
                BulletGlobals.SIMD_PI * 0.5f);
        joint6DOF.setAngularUpperLimit(tmp);
      }

      joints[JOINT_LEFT_SHOULDER] = joint6DOF;

      ownerWorld.addConstraint(joints[JOINT_LEFT_SHOULDER], true);
    }
    /// *************************** ///

    /// ******* RIGHT SHOULDER ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      localA.origin.set(0.2f * scale_ragdoll, 0.15f * scale_ragdoll, 0f);

      MatrixUtil.setEulerZYX(localB.basis, 0, 0, BulletGlobals.SIMD_HALF_PI);
      localB.origin.set(0f, -0.18f * scale_ragdoll, 0f);

      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_SPINE],
              bodies[BODYPART_RIGHT_UPPER_ARM],
              localA, localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_PI * 0.8f,
                -BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_PI * 0.5f);
        joint6DOF.setAngularLowerLimit(tmp);
        tmp.set(BulletGlobals.SIMD_PI * 0.8f,
                BulletGlobals.SIMD_EPSILON,
                BulletGlobals.SIMD_PI * 0.5f);
        joint6DOF.setAngularUpperLimit(tmp);
      }

      joints[JOINT_RIGHT_SHOULDER] = joint6DOF;
      ownerWorld.addConstraint(joints[JOINT_RIGHT_SHOULDER], true);
    }
    /// ******* LEFT ELBOW ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      localA.origin.set(0f, 0.18f * scale_ragdoll, 0f);
      localB.origin.set(0f, -0.14f * scale_ragdoll, 0f);

      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_LEFT_UPPER_ARM],
              bodies[BODYPART_LEFT_LOWER_ARM],
              localA, localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_EPSILON);
        joint6DOF.setAngularLowerLimit(tmp);

        tmp.set(BulletGlobals.SIMD_PI * 0.7f,
                BulletGlobals.SIMD_EPSILON,
                BulletGlobals.SIMD_EPSILON);
        joint6DOF.setAngularUpperLimit(tmp);
      }
      //#endif
      joints[JOINT_LEFT_ELBOW] = joint6DOF;
      ownerWorld.addConstraint(joints[JOINT_LEFT_ELBOW], true);
    }
    /// *************************** ///

    /// ******* RIGHT ELBOW ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      localA.origin.set(0f, 0.18f * scale_ragdoll, 0f);
      localB.origin.set(0f, -0.14f * scale_ragdoll, 0f);

      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_RIGHT_UPPER_ARM],
              bodies[BODYPART_RIGHT_LOWER_ARM],
              localA,
              localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_EPSILON);
        joint6DOF.setAngularLowerLimit(tmp);

        tmp.set(BulletGlobals.SIMD_PI * 0.7f,
                BulletGlobals.SIMD_EPSILON,
                BulletGlobals.SIMD_EPSILON);
        joint6DOF.setAngularUpperLimit(tmp);
      }

      joints[JOINT_RIGHT_ELBOW] = joint6DOF;
      ownerWorld.addConstraint(joints[JOINT_RIGHT_ELBOW], true);
    }
    /// ******* PELVIS ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      MatrixUtil.setEulerZYX(localA.basis, 0, BulletGlobals.SIMD_HALF_PI, 0);
      localA.origin.set(0f, 0.15f * scale_ragdoll, 0f);

      MatrixUtil.setEulerZYX(localB.basis, 0, BulletGlobals.SIMD_HALF_PI, 0);
      localB.origin.set(0f, -0.15f * scale_ragdoll, 0f);

      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_PELVIS],
              bodies[BODYPART_SPINE],
              localA, localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_PI * 0.2f,
                -BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_PI * 0.3f);
        joint6DOF.setAngularLowerLimit(tmp);

        tmp.set(BulletGlobals.SIMD_PI * 0.2f,
                BulletGlobals.SIMD_EPSILON,
                BulletGlobals.SIMD_PI * 0.6f);
        joint6DOF.setAngularUpperLimit(tmp);
      }
      joints[JOINT_PELVIS_SPINE] = joint6DOF;
      ownerWorld.addConstraint(joints[JOINT_PELVIS_SPINE], true);
    }
    /// ******* LEFT HIP ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      localA.origin.set(-0.18f * scale_ragdoll, -0.10f * scale_ragdoll, 0f);

      localB.origin.set(0f, 0.225f * scale_ragdoll, 0f);

      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_PELVIS],
              bodies[BODYPART_LEFT_UPPER_LEG],
              localA, localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_HALF_PI * 0.5f,
                -BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_EPSILON);
        joint6DOF.setAngularLowerLimit(tmp);

        tmp.set(BulletGlobals.SIMD_HALF_PI * 0.8f,
                BulletGlobals.SIMD_EPSILON,
                BulletGlobals.SIMD_HALF_PI * 0.6f);
        joint6DOF.setAngularUpperLimit(tmp);
      }

      joints[JOINT_LEFT_HIP] = joint6DOF;
      ownerWorld.addConstraint(joints[JOINT_LEFT_HIP], true);
    }
    /// ******* RIGHT HIP ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      localA.origin.set(0.18f * scale_ragdoll, -0.10f * scale_ragdoll, 0f);
      localB.origin.set(0f, 0.225f * scale_ragdoll, 0f);

      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_PELVIS],
              bodies[BODYPART_RIGHT_UPPER_LEG],
              localA, localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_HALF_PI * 0.5f,
                -BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_HALF_PI * 0.6f);
        joint6DOF.setAngularLowerLimit(tmp);

        tmp.set(BulletGlobals.SIMD_HALF_PI * 0.8f,
                BulletGlobals.SIMD_EPSILON,
                BulletGlobals.SIMD_EPSILON);

        joint6DOF.setAngularUpperLimit(tmp);
      }
      joints[JOINT_RIGHT_HIP] = joint6DOF;
      ownerWorld.addConstraint(joints[JOINT_RIGHT_HIP], true);
    }
    /// ******* LEFT KNEE ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      localA.origin.set(0f, -0.225f * scale_ragdoll, 0f);
      localB.origin.set(0f, 0.185f * scale_ragdoll, 0f);
      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_LEFT_UPPER_LEG],
              bodies[BODYPART_LEFT_LOWER_LEG],
              localA, localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_EPSILON);
        joint6DOF.setAngularLowerLimit(tmp);

        tmp.set(BulletGlobals.SIMD_PI * 0.7f,
                BulletGlobals.SIMD_EPSILON,
                BulletGlobals.SIMD_EPSILON);
        joint6DOF.setAngularUpperLimit(tmp);
      }
      joints[JOINT_LEFT_KNEE] = joint6DOF;
      ownerWorld.addConstraint(joints[JOINT_LEFT_KNEE], true);
    }
    /// ******* RIGHT KNEE ******** ///
    {
      localA.setIdentity();
      localB.setIdentity();

      localA.origin.set(0f, -0.225f * scale_ragdoll, 0f);
      localB.origin.set(0f, 0.185f * scale_ragdoll, 0f);
      joint6DOF = new Generic6DofConstraint(
              bodies[BODYPART_RIGHT_UPPER_LEG],
              bodies[BODYPART_RIGHT_LOWER_LEG],
              localA, localB,
              useLinearReferenceFrameA);

      if (mHasRigidJoint)
      {
        joint6DOF.setAngularLowerLimit(rigid_angluar_limits_a);
        joint6DOF.setAngularUpperLimit(rigid_angluar_limits_b);
      }
      else
      {
        tmp.set(-BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_EPSILON,
                -BulletGlobals.SIMD_EPSILON);
        joint6DOF.setAngularLowerLimit(tmp);
        tmp.set(BulletGlobals.SIMD_PI * 0.7f,
                BulletGlobals.SIMD_EPSILON,
                BulletGlobals.SIMD_EPSILON);
        joint6DOF.setAngularUpperLimit(tmp);
      }
      joints[JOINT_RIGHT_KNEE] = joint6DOF;
      ownerWorld.addConstraint(joints[JOINT_RIGHT_KNEE], true);
    }
  }

  public void destroy()
  {
    // Remove all constraints
    for (int i = 0; i < JOINT_COUNT; ++i)
    {
      ownerWorld.removeConstraint(joints[i]);
      //joints[i].destroy();
      joints[i] = null;
    }

    // Remove all bodies and shapes
    for (int i = 0; i < BODYPART_COUNT; ++i)
    {
      ownerWorld.removeRigidBody(bodies[i]);
      bodies[i].destroy();
      bodies[i] = null;
      shapes[i] = null;
    }
  }

  private RigidBody localCreateRigidBody(float mass, Transform startTransform, CollisionShape shape)
  {
    return localCreateRigidBody(mass, startTransform, shape, false);
  }

  private RigidBody localCreateRigidBody(float mass, Transform startTransform,
                                         CollisionShape shape, boolean isKinematicObject)
  {
    return Utils.localCreateRigidBody(ownerWorld, mass, startTransform, shape, isKinematicObject);
  }

}
