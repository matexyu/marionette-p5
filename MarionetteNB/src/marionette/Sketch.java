/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marionette;

import marionette.ragdoll.P5Wrapper;
import processing.core.PApplet;

/**
 *
 * @author Mathieu
 */
public class Sketch extends PApplet {

  static P5Wrapper cP5Wrapper = new P5Wrapper();

  static boolean cIsPresentationMode = false;

  public static void main(String[] args)
  {
    if (cIsPresentationMode)
    {
      PApplet.main(new String[]
      {
        "--present", "marionette.Sketch"
      });
    }
    else
    {
      PApplet.main(new String[]
      {
        "marionette.Sketch"
      });
    }
  }

  boolean mIsFullscreen = false;

  @Override
  public void setup()
  {
    if (mIsFullscreen)
    {
      cP5Wrapper.setup(this, displayWidth, displayHeight);
    }
    else
    {
      cP5Wrapper.setup(this, displayWidth - 8 * 2, displayHeight - 25);
    }
  }

  @Override
  public void draw()
  {
    cP5Wrapper.draw(this);
  }

  @Override
  public void keyPressed()
  {
    cP5Wrapper.keyPressed(key, keyCode);
  }

  // Leap Motion Callbacks
  void leapOnInit()
  {
    println("Leap Motion Init");
  }

  void leapOnConnect()
  {
    println("Leap Motion Connect");
  }

  void leapOnFrame()
  {
  //println("Leap Motion Frame");
  }

  void leapOnDisconnect()
  {
    println("Leap Motion Disconnect");
  }

  void leapOnExit()
  {
    println("Leap Motion Exit");
  }

}
