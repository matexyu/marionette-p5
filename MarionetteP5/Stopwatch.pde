
class Stopwatch {

  boolean mIsFirstTick = true;
  double mUsecsOld = 0.0;
  
  void reset()
  {
      mIsFirstTick = true;
      mUsecsOld = 0.0;
  }

  double getDeltaTimeMicroseconds()
  {
    double now = System.nanoTime() / 1000.0;
    if(mIsFirstTick)
    {
      mIsFirstTick = false;
      return 0.0;
    }
    double dt = now - mUsecsOld;
    mUsecsOld = now;
    return dt;
  }
  
}
