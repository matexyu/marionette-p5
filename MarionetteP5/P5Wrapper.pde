import peasy.PeasyCam;

class P5Wrapper {

PeasyCam cam;
BulletRagdoll mBulletRagdoll;
Stopwatch mTimer = new Stopwatch();
boolean pause = false;
  
public void setup(PApplet p5) 
{
  p5.size(displayWidth - 8*2, displayHeight - 25, P3D);
  p5.frameRate(60);
  
  cam = new PeasyCam(p5, 250);
  
  mBulletRagdoll = new BulletRagdoll(p5);
 
  mBulletRagdoll.initPhysics();
}

public void draw(PApplet p5)
{
  if(p5.frameCount == 1)
  {
     p5.frame.setLocation(8, 8);
     cam.setMinimumDistance(200);
     cam.setDistance(500);
     cam.setMaximumDistance(1000);
  }
 
  p5.background(128);  
  p5.lights();
  
  p5.stroke(0);
  p5.noFill();
  p5.box(400);

  if (pause == false)
  { 
    double dt = mTimer.getDeltaTimeMicroseconds();
    double minFPS = 1000000.0 / 60.0;
    if (dt > minFPS) 
    {
      dt = minFPS;
    }
  
    mBulletRagdoll.stepSimulation((float)(dt / 1000000.0));
    mBulletRagdoll.draw();
  }
  
}
}
