/*

https://github.com/davidB/jmbullet/blob/master/src/test/java/com/bulletphysics/demos/genericjoint/RagDoll.java

*/

import com.bulletphysics.util.ObjectArrayList;

import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.constraintsolver.ConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;

import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.DefaultMotionState;

import javax.vecmath.Vector3f;
import javax.vecmath.Matrix4f;

class BulletRagdoll {
  
  BulletRagdoll(PApplet papplet)
  {
     p5 = papplet;
  }

boolean pause = false;

PApplet p5;

DiscreteDynamicsWorld dynamicsWorld;

ObjectArrayList<RagDoll> ragdolls = new ObjectArrayList<RagDoll>();

  public RigidBody localCreateRigidBody(float mass, Transform startTransform, CollisionShape shape) 
  {
    boolean isDynamic = (mass != 0f);

    Vector3f localInertia = new Vector3f();
    localInertia.set(0f, 0f, 0f);
    if (isDynamic) {
      shape.calculateLocalInertia(mass, localInertia);
    }

    DefaultMotionState myMotionState = new DefaultMotionState(startTransform);
    RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(mass, myMotionState, shape, localInertia);
    rbInfo.additionalDamping = true;
    RigidBody body = new RigidBody(rbInfo);

    dynamicsWorld.addRigidBody(body);

    return body;
  }

public void initPhysics() 
{  
  // Setup the basic world
    DefaultCollisionConfiguration collision_config = new DefaultCollisionConfiguration();

    CollisionDispatcher dispatcher = new CollisionDispatcher(collision_config);

    Vector3f worldAabbMin = new Vector3f(-10000, -10000, -10000);
    Vector3f worldAabbMax = new Vector3f(10000, 10000, 10000);
    BroadphaseInterface overlappingPairCache = new DbvtBroadphase();

    //#ifdef USE_ODE_QUICKSTEP
    //btConstraintSolver* constraintSolver = new OdeConstraintSolver();
    //#else
    ConstraintSolver constraintSolver = new SequentialImpulseConstraintSolver();
    //#endif

    dynamicsWorld = new DiscreteDynamicsWorld(dispatcher, overlappingPairCache, constraintSolver, collision_config);

    dynamicsWorld.setGravity(new Vector3f(0f, -2000.0f, 0f));

    // Setup a big ground box
    {
      CollisionShape groundShape = new BoxShape(new Vector3f(200f, 50f, 200f));
      Transform groundTransform = new Transform();
      groundTransform.setIdentity();
      groundTransform.origin.set(0f, -200f, 0f);
      localCreateRigidBody(0f, groundTransform, groundShape);
    }

    // Spawn one ragdoll
    spawnRagdoll();
  }

public void spawnRagdoll() {
    spawnRagdoll(false);
  }
  
  
  float mRagDollScale = 150.0f;
public void spawnRagdoll(boolean random) 
{
    RagDoll ragDoll = new RagDoll(dynamicsWorld, new Vector3f(0f, 0f, 0f), mRagDollScale);
    ragdolls.add(ragDoll);
}

void stepSimulation(double delta_time_seconds)
{
    if (dynamicsWorld != null) 
    {
      dynamicsWorld.stepSimulation((float)delta_time_seconds);
      // optional but useful: debug drawing
      //dynamicsWorld.debugDrawWorld();
    }
}

void draw()
{

  double seconds = millis() / 1000.0;
 
  background(128);  
  lights();
  //rotateY((float)(seconds * PI * 2.0 / 50.0));
  
  stroke(0);
  noFill();
  box(400);
 
 fill(255);
 stroke(64);
 
 {
   Transform ht = new Transform();
      ht.setIdentity();
      ht.origin.set(
        map(mouseX, 0, width, -200, 200), 
        map(mouseY, 0, height, 200, -200), 
        0f);
      ragdolls.get(0).setHeadTransform(ht);
 }
 
for(RagDoll rd : ragdolls)
  for (RigidBody rb : rd.bodies) 
  {
    Transform trans = new Transform();
    // Get the shape's Transformation
    rb.getMotionState().getWorldTransform(trans);
    // Now we get its transformation matrix
    Matrix4f out = new Matrix4f();
    trans.getMatrix(out);  
    
    pushMatrix();
    scale(1,-1,1);
    // Apply that matrix before drawing the box
    applyMatrix(out.m00, out.m01, out.m02, out.m03, 
                out.m10, out.m11, out.m12, out.m13, 
                out.m20, out.m21, out.m22, out.m23, 
                out.m30, out.m31, out.m32, out.m33);
    fill(200, 200, 200);
    box(mRagDollScale * 0.1,
        mRagDollScale * 0.3,
        mRagDollScale * 0.1);
    popMatrix();
  }
 
}

}

